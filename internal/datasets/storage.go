package datasets

import (
	"archive/zip"
	"compress/bzip2"
	"context"
	"fmt"
	"io"
	"os"

	"go.temporal.io/sdk/activity"
)

type Storage interface {
	Upload(ctx context.Context, f io.Reader, fileName string) (string, error)
	Download(ctx context.Context, fileName string) (io.ReadCloser, int64, error)
}

type TempStorage struct{}

func (b *TempStorage) Upload(ctx context.Context, f io.Reader, fileName string) (string, error) {
	tmpFile, err := os.CreateTemp("", fileName)
	if err != nil {
		return "", err
	}

	if _, err := io.Copy(tmpFile, f); err != nil {
		return "", err
	}

	return tmpFile.Name(), nil
}

func (b *TempStorage) Download(ctx context.Context, fileName string) (io.ReadCloser, int64, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, 0, err
	}

	stat, err := f.Stat()
	if err != nil {
		return nil, 0, err
	}

	return f, stat.Size(), nil
}

type HeartbeatReader struct {
	reader io.Reader
	size   int64
	pos    int64
	ctx    context.Context
}

func NewHeartbeatReader(ctx context.Context, r io.Reader, size int64) *HeartbeatReader {
	return &HeartbeatReader{
		ctx:    ctx,
		reader: r,
		size:   size,
	}
}

func (pr *HeartbeatReader) Read(p []byte) (int, error) {
	n, err := pr.reader.Read(p)
	if err == nil {
		pr.pos += int64(n)

		n := fmt.Sprintf("%.2f", float64(pr.pos)/float64(pr.size)*100)

		activity.RecordHeartbeat(pr.ctx, n)
	}

	return n, err
}

type CompressionReader struct {
	reader io.Reader
	size   int64
	close  func() error
}

func NewCompressionReader(comp Compression, r io.Reader, size int64) (*CompressionReader, error) {
	cp := &CompressionReader{size: size}

	switch comp {
	case CompressionZip:
		achive, err := zip.NewReader(r.(io.ReaderAt), size)
		if err != nil {
			return nil, err
		}

		if len(achive.File) > 0 {
			ar, err := achive.File[0].Open()
			if err != nil {
				return nil, err
			}

			cp.reader = ar
			cp.close = ar.Close
		}

	case CompressionBzip2:
		cp.reader = bzip2.NewReader(r)
	default:
		cp.reader = r
	}

	return cp, nil
}

func (cr *CompressionReader) Read(p []byte) (int, error) {
	return cr.reader.Read(p)
}

func (cr *CompressionReader) Close() error {
	if cr.close != nil {
		return cr.close()
	}

	return nil
}
