package datasets

import (
	"time"

	"github.com/robfig/cron/v3"
)

type Compression string

var (
	CompressionZip   Compression = "zip"
	CompressionBzip2 Compression = "bzip2"
)

func (c Compression) IsValid() bool {
	switch c {
	case CompressionZip, CompressionBzip2:
		return true
	}

	return false
}

type DataSet struct {
	Name         string
	DownloadURL  string
	Compression  Compression
	Priority     int
	Workflow     string
	TaskQueue    string
	Schedule     cron.Schedule
	NextUpdateAt time.Time
}

func (ds *DataSet) SetNextUpdate(t time.Time) {
	ds.NextUpdateAt = ds.Schedule.Next(t)
}
