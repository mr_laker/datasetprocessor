package datasets

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	"go.temporal.io/sdk/activity"
)

type Activities struct {
	Storage Storage
	client  *http.Client
}

func NewActivities(s Storage) *Activities {
	return &Activities{
		Storage: s,
		client:  &http.Client{},
	}
}

type DownloadRequest struct {
	URL         string
	FileName    string
	Compression Compression
}

func (a *Activities) DownloadFile(ctx context.Context, r *DownloadRequest) (string, error) {
	logger := activity.GetLogger(ctx)
	logger.Info("downloading file...", "url", r.URL)

	request, err := http.NewRequestWithContext(ctx, "GET", r.URL, nil)
	if err != nil {
		return "", err
	}

	response, err := a.client.Do(request)
	if err != nil {
		return "", err
	}

	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("bad status: %s", response.Status)
	}

	defer response.Body.Close()

	pr := NewHeartbeatReader(ctx, response.Body, response.ContentLength)

	filePath, err := a.Storage.Upload(ctx, pr, r.FileName)
	if err != nil {
		logger.Error("downloadFile failed to save file.", "Error", err)
		return "", err
	}

	return filePath, nil
}

type ProcessRequest struct {
	FileName    string
	Compression Compression
}

func (a *Activities) ProcessFile(ctx context.Context, data *ProcessRequest) error {
	logger := activity.GetLogger(ctx)
	logger.Info("processing file...", "FileName", data.FileName, "Compression", data.Compression)

	r, size, err := a.Storage.Download(ctx, data.FileName)
	if err != nil {
		return err
	}

	defer r.Close()

	cr, err := NewCompressionReader(data.Compression, r, size)
	if err != nil {
		return err
	}

	defer cr.Close()

	pr := NewHeartbeatReader(ctx, cr, size)

	if _, err := io.Copy(io.Discard, pr); err != nil {
		return err
	}

	for i := 0; i < 5; i++ {
		time.Sleep(time.Second)

		activity.RecordHeartbeat(ctx, i)
	}

	return nil
}
