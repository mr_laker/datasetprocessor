package datasets

import (
	"time"

	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

func OpenStreetMapWorkflow(ctx workflow.Context, r *DownloadRequest) error {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: time.Hour,
		HeartbeatTimeout:    30 * time.Second,
		RetryPolicy: &temporal.RetryPolicy{
			InitialInterval:    time.Second,
			BackoffCoefficient: 2.0,
			MaximumInterval:    time.Minute,
			MaximumAttempts:    3,
		},
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	if err := processDataSet(ctx, r); err != nil {
		workflow.GetLogger(ctx).Error("OpenStreetMapWorkflow failed.", "Error", err.Error())

		return err
	}

	workflow.GetLogger(ctx).Info("OpenStreetMapWorkflow completed.")

	return nil
}

func MobileAdTechWorkflow(ctx workflow.Context, r *DownloadRequest) error {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: time.Hour,
		HeartbeatTimeout:    30 * time.Second,
		RetryPolicy: &temporal.RetryPolicy{
			InitialInterval:    time.Second,
			BackoffCoefficient: 2.0,
			MaximumInterval:    time.Minute,
			MaximumAttempts:    3,
		},
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	if err := processDataSet(ctx, r); err != nil {
		workflow.GetLogger(ctx).Error("MobileAdTechWorkflow failed.", "Error", err.Error())

		return err
	}

	workflow.GetLogger(ctx).Info("MobileAdTechWorkflow completed.")

	return nil
}

func OpenSkyWorkflow(ctx workflow.Context, r *DownloadRequest) error {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: time.Hour,
		HeartbeatTimeout:    30 * time.Second,
		RetryPolicy: &temporal.RetryPolicy{
			InitialInterval:    time.Second,
			BackoffCoefficient: 2.0,
			MaximumInterval:    time.Minute,
			MaximumAttempts:    3,
		},
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	if err := processDataSet(ctx, r); err != nil {
		workflow.GetLogger(ctx).Error("OpenSkyWorkflow failed.", "Error", err.Error())

		return err
	}

	workflow.GetLogger(ctx).Info("OpenSkyWorkflow completed.")

	return nil
}

func MarineCadastreWorkflow(ctx workflow.Context, r *DownloadRequest) error {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: time.Hour,
		HeartbeatTimeout:    30 * time.Second,
		RetryPolicy: &temporal.RetryPolicy{
			InitialInterval:    time.Second,
			BackoffCoefficient: 2.0,
			MaximumInterval:    time.Minute,
			MaximumAttempts:    3,
		},
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	if err := processDataSet(ctx, r); err != nil {
		workflow.GetLogger(ctx).Error("MarineCadastreWorkflow failed.", "Error", err.Error())

		return err
	}

	workflow.GetLogger(ctx).Info("MarineCadastreWorkflow completed.")

	return nil
}

func processDataSet(ctx workflow.Context, r *DownloadRequest) (err error) {
	so := &workflow.SessionOptions{
		CreationTimeout:  time.Minute,
		ExecutionTimeout: time.Hour,
	}
	ctx, err = workflow.CreateSession(ctx, so)
	if err != nil {
		return err
	}

	defer workflow.CompleteSession(ctx)

	var (
		a              *Activities
		downloadedName string
	)

	err = workflow.ExecuteActivity(ctx, a.DownloadFile, r).Get(ctx, &downloadedName)
	if err != nil {
		return err
	}

	pr := &ProcessRequest{
		FileName:    downloadedName,
		Compression: r.Compression,
	}

	return workflow.ExecuteActivity(ctx, a.ProcessFile, pr).Get(ctx, nil)
}
