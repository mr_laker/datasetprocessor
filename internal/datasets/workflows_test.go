package datasets

import (
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"go.temporal.io/sdk/testsuite"
	"go.temporal.io/sdk/worker"
)

type UnitTestSuite struct {
	suite.Suite
	testsuite.WorkflowTestSuite
}

func TestWorkflowsSuite(t *testing.T) {
	suite.Run(t, new(UnitTestSuite))
}

func (s *UnitTestSuite) TestOpenStreetMapWorkflow() {
	env := s.NewTestWorkflowEnvironment()
	env.SetWorkerOptions(worker.Options{
		EnableSessionWorker: true,
	})

	var a *Activities

	r := &DownloadRequest{
		URL:      "http://example.com",
		FileName: "example_123",
	}

	env.OnActivity(a.DownloadFile, mock.Anything, r).Return("/tmp/example_123", nil)

	pr := &ProcessRequest{
		FileName: "/tmp/example_123",
	}

	env.OnActivity(a.ProcessFile, mock.Anything, pr).Return(nil)

	env.RegisterActivity(a)

	env.ExecuteWorkflow(OpenStreetMapWorkflow, r)

	s.True(env.IsWorkflowCompleted())
	s.NoError(env.GetWorkflowError())

	env.AssertExpectations(s.T())
}

func (s *UnitTestSuite) TestOpenSkyWorkflow() {
	env := s.NewTestWorkflowEnvironment()
	env.SetWorkerOptions(worker.Options{
		EnableSessionWorker: true,
	})

	var a *Activities

	r := &DownloadRequest{
		URL:      "http://example.com",
		FileName: "example_123",
	}

	env.OnActivity(a.DownloadFile, mock.Anything, r).Return("/tmp/example_123", nil)

	pr := &ProcessRequest{
		FileName: "/tmp/example_123",
	}

	env.OnActivity(a.ProcessFile, mock.Anything, pr).Return(nil)

	env.RegisterActivity(a)

	env.ExecuteWorkflow(OpenSkyWorkflow, r)

	s.True(env.IsWorkflowCompleted())
	s.NoError(env.GetWorkflowError())

	env.AssertExpectations(s.T())
}

func (s *UnitTestSuite) TestMarineCadastreWorkflow() {
	env := s.NewTestWorkflowEnvironment()
	env.SetWorkerOptions(worker.Options{
		EnableSessionWorker: true,
	})

	var a *Activities

	r := &DownloadRequest{
		URL:      "http://example.com",
		FileName: "example_123",
	}

	env.OnActivity(a.DownloadFile, mock.Anything, r).Return("/tmp/example_123", nil)

	pr := &ProcessRequest{
		FileName: "/tmp/example_123",
	}

	env.OnActivity(a.ProcessFile, mock.Anything, pr).Return(nil)

	env.RegisterActivity(a)

	env.ExecuteWorkflow(MarineCadastreWorkflow, r)

	s.True(env.IsWorkflowCompleted())
	s.NoError(env.GetWorkflowError())

	env.AssertExpectations(s.T())
}
