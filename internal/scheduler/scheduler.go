package scheduler

import (
	"context"
	"fmt"
	"log"
	"sort"
	"time"

	"github.com/google/uuid"
	"go.temporal.io/api/enums/v1"
	"go.temporal.io/sdk/client"

	"gitlab.com/mr_laker/datasetprocessor/internal/datasets"
)

type Scheduler struct {
	temporal     client.Client
	checkIterval time.Duration
	datasets     datasetList
}

func New(tc client.Client, checkIterval time.Duration, datasets []*datasets.DataSet) *Scheduler {
	var schedules []*DataSetSchedule

	for _, ds := range datasets {
		schedules = append(schedules, &DataSetSchedule{DataSet: ds})
	}

	return &Scheduler{
		temporal:     tc,
		checkIterval: checkIterval,
		datasets:     datasetList(schedules),
	}
}

func (s *Scheduler) Create(ctx context.Context) error {
	for _, ds := range s.datasets {
		if err := s.createDataSetSchedule(ctx, ds); err != nil {
			return err
		}
	}

	return nil
}

func (s *Scheduler) createDataSetSchedule(ctx context.Context, ds *DataSetSchedule) error {
	log.Println("creating dataset schedule", "name", ds.Name)

	scheduleID := ds.Name
	workflowID := fmt.Sprintf("schedule_%s_%v", ds.Name, uuid.New().String())

	scheduleHandle, err := s.temporal.ScheduleClient().Create(ctx, client.ScheduleOptions{
		ID:   scheduleID,
		Spec: client.ScheduleSpec{},
		Action: &client.ScheduleWorkflowAction{
			ID:        workflowID,
			Workflow:  ds.Workflow,
			TaskQueue: ds.TaskQueue,
			Args: []any{
				&datasets.DownloadRequest{
					URL:         ds.DownloadURL,
					Compression: ds.Compression,
					FileName:    workflowID,
				},
			},
		},
	})
	if err != nil {
		return err
	}

	ds.ScheduleHandle = scheduleHandle

	return nil
}

func (s *Scheduler) CleanUp(ctx context.Context) error {
	for _, ds := range s.datasets {
		if err := s.removeDataSetSchedule(ctx, ds); err != nil {
			return err
		}
	}

	return nil
}

func (s *Scheduler) removeDataSetSchedule(ctx context.Context, ds *DataSetSchedule) error {
	log.Println("deleting dataset schedule", "name", ds.Name, "scheduleID", ds.ScheduleHandle.GetID())

	return ds.ScheduleHandle.Delete(ctx)
}

func (s *Scheduler) runDataSetWorkflow(ctx context.Context, ds *DataSetSchedule) error {
	log.Println("scheduling workflow", "name", ds.Name, "scheduleID", ds.ScheduleHandle.GetID())

	return ds.ScheduleHandle.Trigger(ctx, client.ScheduleTriggerOptions{
		Overlap: enums.SCHEDULE_OVERLAP_POLICY_SKIP,
	})
}

func (s *Scheduler) Run(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		t := time.Now()

		items := s.datasets.Filter(t)

		log.Println("run scheduler", "datasets", len(items))

		for _, ds := range items {
			if err := s.runDataSetWorkflow(ctx, ds); err != nil {
				log.Printf("run: %v", err)
			}

			ds.SetNextUpdate(t)

			log.Println("dataSet", ds.Name, "nextUpdateAt", ds.NextUpdateAt)
		}

		time.Sleep(s.checkIterval)
	}
}

type DataSetSchedule struct {
	*datasets.DataSet
	ScheduleHandle client.ScheduleHandle
}

type datasetList []*DataSetSchedule

func (ds datasetList) Len() int {
	return len(ds)
}

func (ds datasetList) Less(i, j int) bool {
	return ds[i].Priority > ds[j].Priority
}

func (ds datasetList) Swap(i, j int) {
	ds[i], ds[j] = ds[j], ds[i]
}

func (ds datasetList) Filter(t time.Time) datasetList {
	var items datasetList

	for _, d := range ds {
		if d.NextUpdateAt.Before(t) {
			items = append(items, d)
		}
	}

	sort.Sort(items)

	return items
}
