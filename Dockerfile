FROM golang:1.20.4-bullseye AS devel

ENV APP_PATH  /opt/datasetprocessor

RUN set -ex \
    && buildDeps=' \
        gcc \
        musl-dev \
        git \
        curl \
    ' \
    && apt-get update && apt-get install -y $buildDeps --no-install-recommends \
    && apt-get clean

ENV LINTER_VERSION v1.52.1

RUN set -ex \
    && curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin $LINTER_VERSION

RUN set -ex \
    && go install github.com/go-delve/delve/cmd/dlv@latest

WORKDIR ${APP_PATH}

COPY go.mod go.sum $APP_PATH/
RUN go mod download

COPY . $APP_PATH

RUN make build
