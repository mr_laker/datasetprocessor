package cmd

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/robfig/cron/v3"
	"go.temporal.io/sdk/client"

	"gitlab.com/mr_laker/datasetprocessor/internal/datasets"
	"gitlab.com/mr_laker/datasetprocessor/internal/scheduler"
)

func parseDataSet(name string, ds DataSet) (*datasets.DataSet, error) {
	schedule, err := cron.ParseStandard(ds.Schedule)
	if err != nil {
		return nil, fmt.Errorf("schedule: %s - %w", ds.Schedule, err)
	}

	if !ds.Workflow.IsValid() {
		return nil, fmt.Errorf("workflow is not implemented: %s", ds.Workflow)
	}

	comp := datasets.Compression(ds.Compression)
	if comp != "" && !comp.IsValid() {
		return nil, fmt.Errorf("compression is not supported: %s", ds.Compression)
	}

	return &datasets.DataSet{
		Name:        name,
		DownloadURL: ds.DownloadURL,
		Priority:    ds.Priority,
		Workflow:    string(ds.Workflow),
		TaskQueue:   ds.TaskQueue,
		Compression: comp,
		Schedule:    schedule,
	}, nil
}

func newScheduler(ts client.Client, config *Config) (*scheduler.Scheduler, error) {
	t := time.Now()

	var items []*datasets.DataSet

	for key, d := range config.DataSets {
		ds, err := parseDataSet(key, d)
		if err != nil {
			return nil, err
		}

		if !d.TriggerImmediately {
			ds.SetNextUpdate(t)
		}

		log.Println("parseDataSet", key, "nextUpdateAt", ds.NextUpdateAt)

		items = append(items, ds)
	}

	return scheduler.New(ts, config.Scheduler.CheckIterval, items), nil
}

func Wait() {
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown ...")
}
