package cmd

import (
	"context"
	"log"

	"github.com/spf13/cobra"
	"go.temporal.io/sdk/client"
)

var CmdScheduler = &cobra.Command{
	Use:   "scheduler",
	Short: "run scheduler",
	Run:   runScheduler,
}

var schedulerCfg struct {
	ConfigFile string
}

func init() {
	CmdScheduler.PersistentFlags().StringVar(&schedulerCfg.ConfigFile, "config", "config.yaml", "config file")
}

func runScheduler(c *cobra.Command, args []string) {
	config, err := loadConfig(schedulerCfg.ConfigFile)
	if err != nil {
		log.Fatalln("Unable to load config", err)
	}

	tc, err := client.Dial(client.Options{
		HostPort: config.Temporal.Address,
	})
	if err != nil {
		log.Fatalln("Unable to create client", err)
	}

	s, err := newScheduler(tc, config)
	if err != nil {
		log.Fatalln("Unable to create scheduler", err)
	}

	if err := s.Create(context.Background()); err != nil {
		log.Fatalln("Unable to create schedules", err)
	}

	ctx, cancel := context.WithCancel(context.Background())

	go s.Run(ctx)

	Wait()

	cancel()

	if err := s.CleanUp(context.Background()); err != nil {
		log.Fatalln("Unable to remove schedules", err)
	}

	tc.Close()
}
