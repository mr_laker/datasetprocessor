package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"

	"gitlab.com/mr_laker/datasetprocessor/internal/datasets"
)

var CmdWorker = &cobra.Command{
	Use:   "worker",
	Short: "run worker",
	Run:   runWorker,
}

var workerCfg struct {
	ConfigFile           string
	TaskQueue            string
	ConcurrentActivities int
	ConcurrentWorkflow   int
}

func init() {
	CmdWorker.PersistentFlags().StringVar(&workerCfg.ConfigFile, "config", "config.yaml", "config file")
	CmdWorker.PersistentFlags().StringVar(&workerCfg.TaskQueue, "taskQueue", "datasets", "task queue")
	CmdWorker.PersistentFlags().IntVar(&workerCfg.ConcurrentActivities, "ca", 4, "max concurrent activity executions")
	CmdWorker.PersistentFlags().IntVar(&workerCfg.ConcurrentWorkflow, "cw", 2, "max concurrent workflow task executions")
}

func runWorker(c *cobra.Command, args []string) {
	config, err := loadConfig(workerCfg.ConfigFile)
	if err != nil {
		log.Fatalln("Unable to load config", err)
	}

	tc, err := client.Dial(client.Options{
		HostPort: config.Temporal.Address,
	})
	if err != nil {
		log.Fatalln("Unable to create client", err)
	}

	defer tc.Close()

	workerOptions := worker.Options{
		EnableSessionWorker:                    true,
		MaxConcurrentActivityExecutionSize:     workerCfg.ConcurrentActivities,
		MaxConcurrentWorkflowTaskExecutionSize: workerCfg.ConcurrentWorkflow,
	}

	w := worker.New(tc, workerCfg.TaskQueue, workerOptions)

	w.RegisterWorkflow(datasets.OpenStreetMapWorkflow)
	w.RegisterWorkflow(datasets.MobileAdTechWorkflow)
	w.RegisterWorkflow(datasets.OpenSkyWorkflow)
	w.RegisterWorkflow(datasets.MarineCadastreWorkflow)

	w.RegisterActivity(datasets.NewActivities(&datasets.TempStorage{}))

	if err = w.Run(worker.InterruptCh()); err != nil {
		log.Fatalln("Unable to start worker", err)
	}
}
