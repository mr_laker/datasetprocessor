package cmd

import (
	"time"

	"github.com/spf13/viper"
)

type Temporal struct {
	Address string
}

type Workflow string

func (w Workflow) IsValid() bool {
	switch w {
	case "OpenStreetMapWorkflow", "MobileAdTechWorkflow", "OpenSkyWorkflow", "MarineCadastreWorkflow":
		return true
	}

	return false
}

type DataSet struct {
	DownloadURL        string
	Priority           int
	Schedule           string
	Workflow           Workflow
	TaskQueue          string
	TriggerImmediately bool
	Compression        string
}

type Scheduler struct {
	CheckIterval time.Duration
}

type Config struct {
	Temporal  Temporal
	DataSets  map[string]DataSet
	Scheduler Scheduler
}

func loadConfig(path string) (*Config, error) {
	var conf Config

	viper.SetConfigFile(path)

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := viper.Unmarshal(&conf); err != nil {
		return nil, err
	}

	return &conf, nil
}
