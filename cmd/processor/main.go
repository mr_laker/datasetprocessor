package main

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/mr_laker/datasetprocessor/cmd/processor/cmd"
)

var rootCmd = &cobra.Command{
	Use:   "processor",
	Short: "dataset processor",
}

func init() {
	rootCmd.AddCommand(cmd.CmdWorker)
	rootCmd.AddCommand(cmd.CmdScheduler)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
