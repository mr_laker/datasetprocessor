
## To run:
	docker compose up -d

> **Note:** In case when temporal startup takes some time and workers and scheduler can't connect to it, try to execute the command again.


## To run tests:
	docker compose run --rm -it scheduler make test

## To build:
	docker compose run --rm -it scheduler make build
